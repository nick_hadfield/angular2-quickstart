import { Component, OnInit } from '@angular/core';
import { ICustomer } from './ICustomer';

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ]
})
export class HomeComponent {

  constructor() {
    this.customers.push({ firstname: 'Nick', surname: 'Hadfield' });
    this.customers.push({ firstname: 'Joe', surname: 'Bloggs' });
    this.customers.push({ firstname: 'John', surname: 'Smith' });
  }

  customers: ICustomer[] = [];

  slices: any[] = [
    {
      name: 'Microsoft Internet Explorer',
      y: 56.33
    }, {
      name: 'Chrome',
      y: 24.03,
      sliced: true,
      selected: true
    }, {
      name: 'Firefox',
      y: 10.38
    }, {
      name: 'Safari',
      y: 4.77
    }, {
      name: 'Opera',
      y: 0.91
    }, {
      name: 'Proprietary or Undetectable',
      y: 0.2
    }
  ];
}


