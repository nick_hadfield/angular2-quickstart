import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiModule } from '../ui/ui.module';

// Tabs //
import { HomeComponent } from './home/home.component';

// Table //

@NgModule({
  imports: [
    CommonModule,
    UiModule
  ],
  declarations: [
    HomeComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class PageModule { }
