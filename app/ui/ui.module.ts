import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Panel //
import { AvPanelComponent } from './panel/panel.component';

// Tabs //
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab.component';

// Table //
import { AvTableComponent } from './table/av-table.component';
import { AvColumnComponent } from './table/av-column.component';
import { AvHeadComponent } from './table/av-head.component';
import { AvLineComponent } from './table/av-line.component';
import { AvFootComponent } from './table/av-foot.component';

// Charts //
import { AvPieChartComponent } from './chart/pie-chart.component';
import { AvLineChartComponent } from './chart/line-chart.component';
import { AvScatterPlotComponent } from './chart/scatter-plot.component';
import { AvDynamicLineChartComponent } from './chart/dynamic-line-chart.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TabsComponent,
    TabComponent,
    AvTableComponent,
    AvColumnComponent,
    AvHeadComponent,
    AvLineComponent,
    AvFootComponent,
    AvPanelComponent,
    AvPieChartComponent,
    AvLineChartComponent,
    AvScatterPlotComponent,
    AvDynamicLineChartComponent

  ],
  exports: [
    TabsComponent,
    TabComponent,
    AvTableComponent,
    AvColumnComponent,
    AvHeadComponent,
    AvLineComponent,
    AvFootComponent,
    AvPanelComponent,
    AvPieChartComponent,
    AvLineChartComponent,
    AvScatterPlotComponent,
    AvDynamicLineChartComponent
  ]
})
export class UiModule { }
