import { Component, Input, TemplateRef, ContentChild, ViewChild } from '@angular/core';
import { AvColumnComponent } from "./av-column.component";

@Component({
  selector: 'av-line',
  moduleId: module.id,
  template: '<template let-row="row"> <span *ngIf="field">{{row[field]}}</span> <ng-content></ng-content></template>'
})
export class AvLineComponent {

  @Input() field: string;
  @ViewChild(TemplateRef) viewTemplate: TemplateRef<any>;
  @ContentChild(TemplateRef) contentTemplate: TemplateRef<any>;

  get template(): TemplateRef<any> {
     return this.contentTemplate || this.viewTemplate;
  }

  constructor(
    private column: AvColumnComponent
  ) {
    column.body = this;
  }
}
