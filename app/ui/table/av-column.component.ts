import { Component, Input, Output, OnInit, EventEmitter, ViewChild, ViewContainerRef } from '@angular/core';
import { AvTableComponent } from './av-table.component';
import { AvHeadComponent } from './av-head.component';
import { AvLineComponent } from './av-line.component';
import { AvFootComponent } from './av-foot.component';
import { IColumn } from './IColumn';


@Component({
  selector: 'av-column',
  moduleId: module.id,
  template: ''
})
export class AvColumnComponent implements IColumn {

  @Input() title: string;

  //@ViewChild(AvHeadComponent)
  head: AvHeadComponent;

  //@ViewChild(AvBodyComponent)
  body: AvLineComponent;

  //@ViewChild(AvFootComponent)
  foot: AvFootComponent;

  constructor(table: AvTableComponent) {
    table.addColumn(this);
  }

}
