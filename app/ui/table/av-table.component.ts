import { Component, Input } from '@angular/core';
import { AvColumnComponent } from './av-column.component';

@Component({
  selector: 'av-table',
  moduleId: module.id,
  templateUrl: 'av-table.component.html'
})
export class AvTableComponent {

  @Input() showHeader: boolean = true;
  @Input() showFooter: boolean = false;

  @Input() columns: AvColumnComponent[] = [];
  @Input() rows: any[] = []

  addColumn(column: AvColumnComponent) {
    this.columns.push(column);
  }

}
