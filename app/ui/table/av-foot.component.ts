import { Component, Input, ViewContainerRef, ElementRef, TemplateRef, ContentChild, ViewChild } from '@angular/core';
import { AvColumnComponent } from "./av-column.component";

@Component({
  selector: 'av-foot',
  moduleId: module.id,
  template: '<template><ng-content></ng-content></template>'
})
export class AvFootComponent {

  @ViewChild(TemplateRef) template: TemplateRef<any>;

  constructor(
    private column: AvColumnComponent
  ) {
    column.foot = this;
  }

}
