import { Component, Input, Output, OnInit, EventEmitter, TemplateRef, ViewChild } from '@angular/core';
import { AvColumnComponent } from "./av-column.component";

@Component({
  selector: 'av-head',
  moduleId: module.id,
  template: '<template><ng-content></ng-content></template>'
})
export class AvHeadComponent {

  @ViewChild(TemplateRef) template: TemplateRef<any>;

  constructor(
    private column: AvColumnComponent
  ) {
    column.head = this;
  }

}
