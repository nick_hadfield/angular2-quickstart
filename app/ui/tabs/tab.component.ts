import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { TabsComponent } from './tabs.component';


@Component({
  selector: 'av-tab',
  moduleId: module.id,
  templateUrl: 'tab.component.html'
})
export class TabComponent {

  @Input() title: string;
  @Input() selected: boolean;

  constructor(
    private tabs: TabsComponent
  ) {
    tabs.addTab(this);
  }
}
