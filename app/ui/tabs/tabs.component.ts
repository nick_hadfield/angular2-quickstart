import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { TabComponent } from './tab.component';

@Component({
  selector: 'av-tabs',
  moduleId: module.id,
  templateUrl: 'tabs.component.html'
})
export class TabsComponent {

  tabs: TabComponent[] = [];
  selectedTab: TabComponent;

  addTab(tab:TabComponent) {
    this.tabs.push(tab);
    if (!this.selectedTab) this.selectTab(tab);
  }

  selectTab(tab: TabComponent) {
    this.selectedTab = tab;
    for(let t of this.tabs) {
      t.selected = t == this.selectedTab;
    }
  }
}
