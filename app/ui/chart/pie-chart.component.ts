import { Component, Input, AfterViewInit, OnInit } from '@angular/core';
import { CHART_DIRECTIVES, HighchartsOptions, HighchartsChartObject } from 'angular2-highcharts';


@Component({
  selector: 'av-pie-chart',
  moduleId: module.id,
  directives: [CHART_DIRECTIVES],
  template: `
        <chart #chart [options]="options"></chart>
    `
})
export class AvPieChartComponent implements OnInit {



  @Input() slices: any[] = [];

  chart : HighchartsChartObject;

  options: HighchartsOptions;

  ngOnInit(): void {
    this.options = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Browser market shares January, 2015 to May, 2015'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'

          }
        }
      },
      series: [{
        name: 'Brands',
        colorByPoint: true,
        data: this.slices
      }]
    };
  }

}
